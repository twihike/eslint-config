# eslint-config

## Installation

With yarn:

```shell
npm install --save-dev eslint @tkhiking/eslint-config
npm install --save-dev prettier @tkhiking/prettier-config
```

With npm:

```shell
yarn add --dev eslint @tkhiking/eslint-config
yarn add --dev prettier @tkhiking/prettier-config
```

## Usage

Edit `package.json`:

```json
{
  "name": "your-cool-library",
  "version": "0.1.0",
  "eslintConfig": {
    "extends": "@tkhiking"
  }
}
```

Or `eslintrc.js`:

```javascript
module.exports = {
  extends: '@tkhiking',
};
```

## License

This project is licensed under the terms of the MIT license.

Copyright (c) 2020 tkhiking.
